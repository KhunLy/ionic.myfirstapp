import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  endPoint = "https://api.openweathermap.org/data/2.5/forecast";
  latitude: number = 35;
  longitude: number = 139;
  key: string = "d52e50e34214ff0b92247f788638eeb9";
  prevision: Prevision;

  constructor(
    private httpClient: HttpClient
  ) {
    httpClient.get<Requete>(
      `${this.endPoint}?lat=${this.latitude}&lon=${this.longitude}&appid=${this.key}`
    )
      .subscribe(data => {
        this.prevision = data.list[0];
      });
  }
}

import { Component } from '@angular/core';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  article: string;
  articles: Array<Article> = [];

  constructor(
    private actionSheetService: ActionSheetController,
    private toastService: ToastController,
    private storage: Storage
  ) {
    this.storage.get("articles").then((data) => {
      // coalesce
      this.articles = data || [];
    });
  }

  ajouterArticle() {
    if(this.article == null || this.article == '')
    {
      return;
    }
    this.articles.push({nom: this.article, coche: false});
    this.article = null;
    this.save();
  }

  async afficherActions(article: Article){
    let actionsSheet = await this.actionSheetService.create({
      buttons: [{
        // ternaire
        // condition ? todo1 : todo2
        text: article.coche ? "Decocher" : "Cocher",
        icon: "checkmark",
        handler: () => { 
          article.coche = !article.coche;
          this.save();
          for(let art of this.articles){
            if(!art.coche)
              return;
          }
          // if(this.articles.filter(item => !item.coche).length == 0)
          this.afficherToast();
        }
      },{
        text: "Supprimer",
        icon: "trash",
        role: "destructive",
        handler: () => {
          let ind = this.articles.indexOf(article);
          this.articles.splice(ind,1);
          this.save();
        }
      }]
    });
    actionsSheet.present();
  }

  async afficherToast() {
    let toast = await this.toastService.create(
      {message: "job done", duration: 5000}
    );
    toast.present();
  }

  save() {
    this.storage.set('articles', this.articles);
  }
}
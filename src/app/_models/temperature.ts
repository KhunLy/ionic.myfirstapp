interface Temperature {
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
}
interface Prevision {
    dt_txt: string;
    main: Temperature;
    weather: Array<Image>;
    wind: Vent;
}